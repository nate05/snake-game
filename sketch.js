//snake game
//Nathan Poirier
//17-09-2018

var x = [100];
var y = [100];
var s = [];
var Width = 512;
var Height = 512;
var Size = 10;
var foodx = 200;
var foody = 200;
var snakeLength = 0;
var Manger = 0;

function setup() {
  createCanvas(Width, Height);
  frameRate(15);
}

function Move() {
  if (keyIsDown(LEFT_ARROW)) {
    if (snakeLength == 0){
      x[0] = x[0] - Size;
    }
    else {
      x.unshift(x[0]-Size);
      x.pop();
      y.unshift(y[0]);
      y.pop();
    }
    Manger = 0;
  }

  else if (keyIsDown(RIGHT_ARROW)) {
    if (snakeLength == 0){
      x[0] = x[0] + Size;
    }
    else {
      x.unshift(x[0]+Size);
      x.pop();
      y.unshift(y[0]);
      y.pop();
    }
    Manger = 0;
  }

  else if (keyIsDown(UP_ARROW)) {
    if (snakeLength == 0){
      y[0] = y[0] - Size;
    }
    else {
      y.unshift(y[0]-Size);
      y.pop();
      x.unshift(x[0]);
      x.pop();
    }
    Manger = 0;
  }

  else if (keyIsDown(DOWN_ARROW)) {
    if (snakeLength == 0){
      y[0] = y[0] + Size;
    }
    else {
      y.unshift(y[0]+Size);
      y.pop();
      x.unshift(x[0]);
      x.pop();
    }
    Manger = 0;
  }
}

function Collision(){
  if (Manger == 0){
    for (i = 0; i <= snakeLength; i++){
      for (j = 0; j <= snakeLength; j++){
        if (i != j){
          if ((x[i] == x[j]) && (y[i] == y[j])){
            console.log("COLLISION");
          }
        }
      }
    }
  }
}


function Snake() {
  for (i = 0; i <= snakeLength; i++){
    s[i] = rect(x[i], y[i], Size, Size);
  //  s[i].fill(255, 0, 0);
  }
  Move();
  if ((x[0] == foodx) && (y[0] == foody)) {
    Eat();
  }
  Collision();
}

function Food() {
  f = rect(foodx, foody, Size, Size);
  fill(0, 255, 0);
}

function Eat() {
  x.push(foodx);
  y.push(foody);
  foodx = round((random(0, Width-Size))/10)*10;
  foody = round((random(0, Height-Size))/10)*10;
  snakeLength += 1;
  Manger = 1;
}

function draw() {
  clear();
  background(153);
  Snake();
  Food();
}
